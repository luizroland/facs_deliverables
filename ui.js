/* I will be using Jquery for this exercise */
/* global $ */
/* global itemsActive */
/* global itemsDeleted */
/* global userId */
// So my linter wont bug me :)

/*
 * Global last added number in the sequence
 * it is set on startUI
*/
var lastAddedNumber;

/*
 * A function that will add a new item to the UI
 * it will update the UI appending a new item to the box */
function addItem(itemId) {
    $('<div class="facs-item" data="' + itemId + '">Item #' + itemId + '</div>')
        .click(function() {
            var cssClass = $(this).attr('class');
            var isItemSelected = cssClass == 'facs-item-selected' ? true : false;

            // I opted for the user be able to select multiple items
            // it could be easily changed to only one
            // Since i didn't read exatly the user can only select one
            // i tend to opt giving the user more power to control his bulk
            if(isItemSelected) {
                $(this).removeClass('facs-item-selected');
                $(this).addClass('facs-item');

            } else {
                $(this).removeClass('facs-item');
                $(this).addClass('facs-item-selected');
            }

            // If we have any item selected we enable delete
            var isAnyItemSelected = $('.facs-item-selected').length > 0;
            if(isAnyItemSelected) {
                $('#delete-button').removeClass('facs-button-disabled');
                $('#delete-button').addClass('facs-button-active');
                $('#delete-button').prop('disabled', false);

            } else {
                $('#delete-button').addClass('facs-button-disabled');
                $('#delete-button').removeClass('facs-button-active');
                $('#delete-button').prop('disabled', true);
            }

        }).appendTo('#items');

    // If it should be visible And is hidden we remove the hidden class
    var shouldDeleteButtonBeVisible = $('#items > div').length > 0;
    if(shouldDeleteButtonBeVisible && $('#delete-button').hasClass('hidden')) {
        $('#delete-button').removeClass('hidden');
    }
}

/*
 * A function that will delete an item on the UI
 * It will receive a item ID and set the status to false
 * then it will update the UI removing the item from the first box
 * and rendering a new one on the second box with updated text node */
function deleteItem(itemId) {
    $('#items > div[data="' + itemId + '"]').remove();
    $('<div class="facs-item" data="' + itemId + '">Item #' + itemId + '</div>')
        .appendTo('#items-deleted');

    // If it should be visible And is hidden we remove the hidden class
    var shouldDeleteButtonBeVisible = $('#items > div').length > 0;
    if(!shouldDeleteButtonBeVisible) {
        $('#delete-button').addClass('hidden');
        $('#delete-button').addClass('facs-button-disabled');
    }

    // Delete is always disabled after deleting since no item will be selected
    // and the items deleted will also be shown
    $('#delete-button').addClass('facs-button-disabled');
    $('#delete-button').removeClass('facs-button-active');
    $('#delete-button').prop('disabled', true);
    $('#items-deleted').removeClass('hidden');
}

/*
 * A function that will call the correponding backend
 * it will receive the item text and description
 * check if the item was created successfully
 * if so it will update the UI */
function saveItem(itemText, itemDesc) {
    // At first i was handling the error by denying the action but since the UI
    // have to work even without the connection i will do the same code
    // on the fail callback trusting the itemText parameter
    $.ajax({
        type: 'POST',
        url: '/test/save-new-item',
        dataType: 'json',
        data: {
            item_text: itemText,
            item_description: itemDesc,
        }
    }).done(function(data) {
        // If we save successfully add new item to the UI
        var itemId = data.item_id;
        addItem(itemId);

    }).fail(function(error) {
        console.log(error);
        console.log('Error saving new item');
        addItem(parseInt(itemText));
    });
}

/*
 * A function that will call the correponding backend
 * it will receive the item id, user id and the new status
 * check if the item was updated successfully
 * if so it will update the UI */
function updateUserItem(itemId, itemStatus) {

    // At first i was handling the error by denying the action but since the UI
    // have to work even without the connection i will do the same code
    // on the fail callback trusting solely on the itemId parameter
    $.ajax({
        type: 'POST',
        url: '/test/update-user-item',
        dataType: 'json',
        data: {
            itemId: itemId,
            userId: userId,
            itemStatus: itemStatus,
        }
    }).done(function(data) {
        var record = data.update_result;
        // If we set the status to false we can the delete
        if(itemStatus == false) {
            deleteItem(record.id);

        // Otherwise we add the item back to the first box
        // I use an else if cause sometimes stuff can be undefined
        // so we actually check if we are doing the right thing here
        } else if(itemStatus) {
            addItem(record.id);
        }

    }).fail(function(error) {
        console.log(error);
        console.log('Error updating item: ' + itemId);
        if(itemStatus == false) {
            deleteItem(itemId);

        } else if(itemStatus) {
            addItem(itemId);
        }
    });
}

/*
 * A function that will read our variables defined in the phtml
 * define the UI status for the first render
 */
function startUI() {
    var activeItems = JSON.parse(itemsActive);
    var deletedItems = JSON.parse(itemsDeleted);
    var lastDeleted;
    var lastActive;

    if(activeItems.length > 0 && deletedItems.length > 0) {
        lastDeleted = deletedItems[activeItems.length-1];
        lastActive = activeItems[activeItems.length-1];

        // Whichever has the bigger id was the last added since we userd ORDER BY id
        var lastAdded = lastDeleted.id > lastActive.id ? lastDeleted : lastActive;
        lastAddedNumber = parseInt(lastAdded.text);

    } else if(activeItems.length > 0 && deletedItems.length == 0) {
        lastActive = activeItems[activeItems.length-1];
        lastAddedNumber = parseInt(lastActive.text);

    } else if(activeItems.length == 0 && deletedItems.length > 0) {
        lastDeleted = deletedItems[deletedItems.length-1];
        lastAddedNumber = parseInt(lastDeleted.text);

    } else {
        lastAddedNumber = 0;
    }

    activeItems.map(function(element) {
        addItem(parseInt(element.text));
    });

    deletedItems.map(function(element) {
        deleteItem(parseInt(element.text));
    });

    $('#add-button').click(function() {
        // As i didn't see a need for the text or description
        // and for the lack of user input i will send empty strings
        lastAddedNumber++;
        saveItem(lastAddedNumber, '');
    });

    $('#delete-button').click(function() {
        $('#items > div.facs-item-selected[data]').each(function() {
            var itemId = $(this).attr('data');
            updateUserItem(itemId, false);
        });

    });
}

// We start at the insertion in the phtml ( for this to happen the script must
// be appended at the end of the website since the main parts of the DOM have
// to be loaded)
startUI();
