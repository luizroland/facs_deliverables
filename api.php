<?php

use Illuminate\Http\Request;

use DB;

//Item Model
use Item;
//Item User Relation Model
use ItemUserRelation;

class APIClass
{
    /*
     * A function that saves a new item
     * It receives a text and description
     * Returns the id of the created item */
    public function saveNewItem(Request $request) {
        $itemText = $request->item_text;
        $itemDesc = $request->item_description;

        $user = $_SESSION['user'];
        /*
         * Assuming
         * user = [
         *     id => integer
         *     name => string
         *     email => string
         * ];
         * */
        $userId = $user->id;

        $newItem = Item::create([
            'text' => $itemText,
            'description' => $itemDesc,
        ]);

        $newItemUserRelation = ItemUserRelation::create([
            'user_id' => $userId,
            'item_id' => $newItem->id,
            'status' => true,
        ]);

        return response()->json([
            'item_id' => $newItem->id
        ]);
    }

    /*
     * A function that updates an existing item
     * It receives the item id, the user id of the owner and the new status
     * Returns the updated item */
    public function updateUserItem(Request $request) {
        $itemId = $request->item_id;
        $itemStatus = $request->item_status;
        $requestUserId = $request->user_id;

        $user = $_SESSION['user'];
        /*
         * Assuming
         * user = [
         *     id => integer
         *     name => string
         *     email => string
         * ];
         * */
        $userId = $user->id;

        // If the user logged request to update another user Item we return 401
        if($userId != $requestUserId) {
            return response()->json([], 401);
        }

        // So if the relation dont exist we return 404
        $itemUserRelation = ItemUserRelation::where('item_id', $itemId)
            ->where('user_id', $userId)
            ->firstOrFail();

        $itemUserRelation->update([
            'status' => $itemStatus,
        ]);

        $updatedItem = Item::where('id', $itemId);

        // I understood that i should return the record but reading again
        // i thought it could mean the literal update status like
        // if it failed or succedded, i will go with my gut and return
        // the record cause it makes more sense for a UI to receive
        // the new record than a yes or no in my head
        // since i think on a more functional approach to UI rendering
        return response()->json([
            'update_result' => $updatedItem
        ]);
    }
}
